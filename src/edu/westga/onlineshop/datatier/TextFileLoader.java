package edu.westga.onlineshop.datatier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * TextFileLoader loads the contents of a specified text file into memory.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class TextFileLoader implements TextLoader {

	private final File aTextFile;
	
	/**
	 * Creates a new TextFileLoader for input of the specified text file.
	 * 
	 * @precondition aTextFile != null
	 * @postcondition the object is ready to read the file
	 * 
	 * @param aTextFile
	 *            the I/O file
	 */
	public TextFileLoader(File aTextFile) {
		if (aTextFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.aTextFile = aTextFile;
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.onlineshopper.datatier.TextLoader#loadText()
	 */
	@Override
	public List<String> loadText() throws IOException {
		return Files.readAllLines(this.aTextFile.toPath());
	}

}
