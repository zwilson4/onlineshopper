package edu.westga.onlineshop;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;


/**
 * ShopApplication extends the JavaFX Application class to build the GUI
 * and start program execution.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class ShopApplication extends Application {
	private static final String WINDOW_TITLE = "Online Shopping Simulator";
	private static final String GUI_FXML = "view/ShopGui.fxml";

	/**
	 * Constructs a new MoviesApplication object that extends the
	 * Application class to build the GUI and start program execution.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute.
	 */
	public ShopApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();

		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}

	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}

}

