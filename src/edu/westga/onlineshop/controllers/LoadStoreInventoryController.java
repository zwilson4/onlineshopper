package edu.westga.onlineshop.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.onlineshop.datatier.TextFileLoader;
import edu.westga.onlineshop.datatier.TextLoader;
import edu.westga.onlineshop.model.DataMap;
import edu.westga.onlineshop.model.ItemData;

/**
 * LoadStoreInventoryController manages the "load store item data" use case:
 * reading store item data from a text file and storing it in the data map.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class LoadStoreInventoryController {
	
	private DataMap<String, ItemData> theMap;
	private File inputFile;

	/**
	 * Creates a new LoadStoreInventoryController to manage adding store items to the specified
	 * map.
	 * 
	 * @precondition inputFile != null && aMap != null
	 * @postcondition the controller is ready to load items
	 * 
	 * @param inputFile
	 *            the file to read
	 * @param aMap
	 *            the data map to which store items will be added
	 */
	public LoadStoreInventoryController(File inputFile,
								   DataMap<String, ItemData> aMap) {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		if (aMap == null) {
			throw new IllegalArgumentException("The map was null");
		}

		this.inputFile = inputFile;
		this.theMap = aMap;
	}

	/**
	 * Loads store item data from the input file into the map.
	 * 
	 * @precondition none
	 * @postcondition the map contains the store item data
	 * 
	 * @throws IOException
	 *             if the file cannot be read
	 */
	public void loadStoreItemData() throws IOException {
		TextLoader loader = new TextFileLoader(this.inputFile);
		List<String> linesFromFile = loader.loadText();

		for (String oneLineFromFile : linesFromFile) {
			this.addMovie(oneLineFromFile);
		}
	}

	private void addMovie(String oneLineFromFile) {
		String[] storeItemDataArray = oneLineFromFile.split(",");
		ItemData data = new ItemData(storeItemDataArray[0],
									   Double.parseDouble(storeItemDataArray[1]));
		this.theMap.add(data.getName(), data);
	}

}