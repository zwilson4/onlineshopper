/**
 * 
 */
package edu.westga.onlineshop.controllers;

import edu.westga.onlineshop.model.CartDataMap;
import edu.westga.onlineshop.model.DataMap;
import edu.westga.onlineshop.model.ItemData;

/**
 * AddToCartController manages the "add store item to cart" use case:
 * adding the selected item to the cart data map.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class AddToCartController {
	
	private CartDataMap cartMap;
	private DataMap<String, ItemData> theStoreMap;
	
	/**
	 * Creates a new AddToCartController to manage adding store items to the specified
	 * map.
	 * 
	 * @precondition aMap != null
	 * @postcondition the controller is ready to add items
	 * 
	 * @param aMap
	 *            the data map to which store items will be added
	 */
	public AddToCartController(DataMap<String, ItemData> aMap) {
		if (aMap == null) {
			throw new IllegalArgumentException("The map was null");
		}
		this.cartMap = new CartDataMap();
		this.theStoreMap = aMap;
	}
	
	/**
	 * Adds the specified store item to the cart map.
	 * 
	 * @precondition	itemMap != null && !getMap().contains(name)
	 * 
	 * @postcondition	getMap().contains(name) && 
	 * 					getMap.size() == getMap.size()@prev + 1 
	 * 
	 * @param item	the storeItem to add
	 */
	public void addItemToCart(String item) {
		if(item == null){
			throw new IllegalArgumentException("Item is null");
		}
		
		if(this.theStoreMap.contains(item)){
			this.cartMap.add(item, this.theStoreMap.get(item));
		}
	}
	
	/**
	 * Returns the DataMap to which inventory items are added.
	 * 
	 * @precondition	none
	 * @return			the map
	 */
	public DataMap<String, ItemData> getMap() {
		return this.theStoreMap;
	}
}
