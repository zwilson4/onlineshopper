package edu.westga.onlineshop.model;

import java.util.List;

/**
 * DataMap defines the interface for classes that 
 * store key/value pairs in memory.
 * 
 * @author Zack Wilson
 * @version Spring, 2015
 * 
 * @param <K>	the class of the keys
 * @param <V>	the class of the values
 */
public interface DataMap<K, V> {
	
	/**
	 * Adds the value with the specified key to this map.
	 * 
	 * @precondition	aKey != null && aValue != null
	 * 					&& !contains(aKey)
	 * @postcondition	contains(aKey) && size() == size()@prev + 1
	 * 
	 * @param aKey		the key to add
	 * @param aValue	the object with the specified key
	 */
	void add(K aKey, V aValue);

	/**
	 * Returns the value associated with the specified key.
	 * 
	 * @precondition	aKey != null
	 * 
	 * @param aKey		the key to access
	 * @return			the value
	 */
	V get(K aKey);
	
	/**
	 * Returns true iff this map contains an object with
	 * the specified key.
	 * 
	 * @precondition	aKey != null
	 * @param aKey		the key to check for
	 * @return			true if the key is found, false otherwise
	 */
	boolean contains(K aKey);
	
	/**
	 * Returns the size of this map.
	 * 
	 * @precondition	none
	 * @return			how many items this map contains
	 */
	int size();

	/** 
	 * Returns a list of the values stored in this map.
	 * 
	 * @precondition	none
	 * @return			the values from the map
	 */
	List<V> values();
	
	/** 
	 * Returns a list of the keys of the values stored in this map.
	 * 
	 * @precondition	none
	 * @return			the keys from the map
	 */
	List<K> keys();
}