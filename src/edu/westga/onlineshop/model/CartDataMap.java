/**
 * 
 */
package edu.westga.onlineshop.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CartDataMap implements the DataMap interface to 
 * store String/StoreItemData pairs in memory
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class CartDataMap implements DataMap<String, ItemData> {
	
	private Map<String, ItemData> theMap;
	
	/**
	 * Creates a new CartDataMap with an empty map.
	 */
	public CartDataMap() {
		this.theMap = new HashMap<>();
	}

	@Override
	public void add(String aKey, ItemData aValue) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (aValue == null) {
			throw new IllegalArgumentException("The value was null");
		}
		if (this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is already in the map");
		}
		
		this.theMap.put(aKey, aValue);
	}

	@Override
	public ItemData get(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (!this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is not in the map");
		}
		
		return this.theMap.get(aKey);
	}

	@Override
	public boolean contains(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		return this.theMap.containsKey(aKey);
	}

	@Override
	public int size() {
		return this.theMap.size();
	}

	@Override
	public List<ItemData> values() {
		return new ArrayList<ItemData>(this.theMap.values());
	}

	@Override
	public List<String> keys() {
		return new ArrayList<String>(this.theMap.keySet());
	}

}