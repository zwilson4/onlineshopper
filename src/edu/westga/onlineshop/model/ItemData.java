package edu.westga.onlineshop.model;

/**
 * StoreItemData objects represent the name and the
 * price of a store item within the store.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class ItemData {
	
	private final String name;
	private final double price;
	
	/**
	 * Creates a new StoreItemData object with the specified
	 * name and price.
	 * 
	 * @precondition	name != null && name.length != 0
	 * 					price > 0
	 * @postcondition	the object exists
	 * 
	 * @param name	the name of the item
	 * @param price	the price of the item
	 */
	public ItemData(String name, double price) {
		if (name == null) {
			throw new IllegalArgumentException("name is null");
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("name is empty");
		}
		if (price <= 0) {
			throw new IllegalArgumentException("price is less than or equal to zero");
		}
		
		this.name = name;
		this.price = price;
	}

	/**
	 * Gets the name of the item
	 * 
	 * @return the name of the item
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the price of the item
	 * 
	 * @return the price of the item
	 */
	public double getPrice() {
		return this.price;
	}

}
