package edu.westga.onlineshop.view;

import java.io.File;
import java.io.IOException;

import edu.westga.onlineshop.controllers.AddToCartController;
import edu.westga.onlineshop.controllers.LoadStoreInventoryController;
import edu.westga.onlineshop.model.DataMap;
import edu.westga.onlineshop.model.ItemData;
import edu.westga.onlineshop.model.StoreItemDataMap;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * OnlineShopperViewModel mediates between the view and the rest of the program.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class OnlineShopViewModel {
	private StringProperty itemTotal;
	
	private DataMap<String, ItemData> itemMap;
	private ObservableList<String> itemNames;
	private ObservableList<String> cartItems;
	private LoadStoreInventoryController loadController;
	private AddToCartController addController;
	
	/**
	 * Creates a new OnlineShopperViewModel object with its properties initialized.
	 * 
	 * @precondition none
	 * @postcondition the object exists
	 */
	public OnlineShopViewModel() {		
		this.itemTotal = new SimpleStringProperty();
		this.itemMap = new StoreItemDataMap();
		this.itemNames = FXCollections.observableArrayList();
		this.cartItems = FXCollections.observableArrayList();
		this.addController = new AddToCartController(this.itemMap);
	}
	
	/**
	 * Returns the property that wraps the total price of store items in the
	 * shopping cart.
	 * 
	 * @precondition none
	 * @return the description string property
	 */
	public StringProperty itemTotalProperty() {
		return this.itemTotal;
	}
	
	/**
	 * Returns the list of the item names in the model.
	 * 
	 * @precondition none
	 * @return		 the titles
	 */
	public ObservableList<String> getNames() {
		return this.itemNames;
	}
	
	/**
	 * Returns the list of the cart item names in the model.
	 * 
	 * @precondition none
	 * @return		 the titles
	 */
	public ObservableList<String> getCartNames() {
		return this.cartItems;
	}
	
	/**
	 * Returns the number of movies that have been added to 
	 * this view model's collection.
	 * 
	 * @precondition none
	 * @return		 how many movies
	 */
	public int size() {
		return this.itemMap.size();
	}
	
	/**
	 * Tells the LoadMovieDataController to import movie data from the
	 * specified file. 
	 * 
	 * @precondition	inputFile != null
	 * @postcondition	the data has been loaded, so that 
	 * 					size() equals the number of lines in the file and
	 * 					getTitles() returns all the titles of the movies in the file
	 * 
	 * @param inputFile	the file to read
	 * @throws IOException 	if the file cannot be read
	 */
	public void loadItemDataFrom(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.loadController = new LoadStoreInventoryController(inputFile, this.itemMap);
		this.loadController.loadStoreItemData();
		
		this.updateItemList();
	}
	
	/**
	 * Adds items to cart
	 * 
	 * @param itemName item to add
	 */
	public void addItemToCartFromInventory(String itemName) {
		this.addController.addItemToCart(itemName);
		this.cartItems.add(itemName);
	}
	
	/**
	 * Gets the total of items to purchase
	 * 
	 * @return total of cartItems
	 */
	public String getTotal(){
		double total = 0.0;
		for(String s : this.getCartNames()){
			total += this.itemMap.get(s).getPrice();
		}
		String textTotal = Double.toString(total);
		return textTotal;
	}

	private void updateItemList() {
		this.itemNames.clear();
		this.itemNames.addAll(this.itemMap.keys());
		this.itemNames.sort(null);
	}

}

