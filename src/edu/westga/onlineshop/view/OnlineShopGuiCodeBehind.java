package edu.westga.onlineshop.view;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * OnlineShopperGuiCodeBehind defines the code-behind "controller" for ShopGui.fxml.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class OnlineShopGuiCodeBehind {
	
	private static final String LOAD_STORE_ITEM_DATA_NAME = 
		      "Load item data";
	private static final String SUCCESSFUL_LOAD_MESSAGE = 
			  "Successfully loaded item data";
	private static final String READ_DATA_ERROR_MESSAGE = 
			  "ERROR: couldn't load the item data";
	private static final String ERROR_DIALOG_TITLE = 
			  "Data import error";

	private OnlineShopViewModel theViewModel;
	
	@FXML
    private ListView<String> inventoryListView;

    @FXML
    private Button checkoutButton;

    @FXML
    private ListView<String> cartListView;

    @FXML
    private Button removeButton;

    @FXML
    private Button addButton;

    @FXML
    private Button getTotalButton;

    @FXML
    private TextField totalTextField;
    
    @FXML
    private MenuItem loadInventoryMenuItem;
    
    @FXML
    private MenuItem exitMenuItem;

    
    /**
	 * Creates a new OnlineShopperGuiCodeBehind object and its view model.
	 * 
	 * @precondition none
	 * @postcondition the object and its view model are ready to be initialized
	 */
    public OnlineShopGuiCodeBehind() {
    	this.theViewModel = new OnlineShopViewModel();
    }
    
    /**
	 * Initializes the GUI component, binding them to the view model properties
	 * and setting their event handlers.
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setEventActions();
	}
	
    private void setEventActions() {
    	this.exitMenuItem.setOnAction(event -> Platform.exit());
    	this.loadInventoryMenuItem.setOnAction(
				     event -> this.handleLoadAction());
    	this.addButton.setOnAction(event -> this.addStoreItemToCartAction());
    	this.getTotalButton.setOnAction(event -> this.displayTotal());
	}

	private void bindComponentsToViewModel() {
		this.inventoryListView.setItems(this.theViewModel.getNames());
	}
	
	private void displayTotal(){
		this.totalTextField.setText(this.theViewModel.getTotal());
	}
	
	private void addStoreItemToCartAction(){
		this.theViewModel.addItemToCartFromInventory(this.inventoryListView.getSelectionModel().getSelectedItem());
		this.cartListView.setItems(this.theViewModel.getCartNames());
	}

	private void handleLoadAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}
		
		try {
			this.theViewModel.loadItemDataFrom(inputFile);
			JOptionPane.showMessageDialog(null,
				    SUCCESSFUL_LOAD_MESSAGE,
				    LOAD_STORE_ITEM_DATA_NAME,
				    JOptionPane.ERROR_MESSAGE);	
		} catch (IOException readException) {
			JOptionPane.showMessageDialog(null,
				    READ_DATA_ERROR_MESSAGE,
				    ERROR_DIALOG_TITLE,
				    JOptionPane.ERROR_MESSAGE);
		}	
	}

	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("Text Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}
    
}